# Serio

[![Clojars Project](https://img.shields.io/clojars/v/serio.svg)](https://clojars.org/serio)

Serio is a time series database built on top of [Apache Kudu](https://kudu.apache.org/).

## Why yet another TSDB?

The first target business domain of Serio is financial time series data. This type of data
has some distinct characteristics compared to domains of monitoring and sensor/IoT data:

* Old data is to be retained as is, not thrown away or rolled up to lower frequencies
* Late updates and out of order observations occur
* Every piece of data can be important, losing some observations is not ok
* TODO: continue on this list
 
The number of available TSDBs has been growing, but no non-commercial DB quite fits the bill perfectly. 
Serio on top of Apache Kudu is not perfect either (see section below with regards to non-ideal
characteristics), but it does tick most of the boxes important to its creator:

* Late updates and out of order inserts are supported
* Both the write and read paths scale out in a reasonable manner avoiding hot spots
* It's possible to see 1mm writes/second on relatively modest hardware requirements
* Data on disk is stored in way that aligns well with time series queries
* Data on disk is encoded and compressed in a reasonable (but not perfect) way
* Supports both time series data and meta data in the same data store
* Data can be indefinitely retained
* It's positioning in terms of the CAP theorem gives suitable guarantees for the business domain at hand

## Rationale for implementing a TSDB on top of Apache Kudu

The best source of general background info on using Kudu for time series can be found in [this excellent presentation](http://schd.ws/hosted_files/apachebigdata2016/fd/Introduction%20to%20Apache%20Kudu.pdf). I don't have much 
to add to that, other than that it satisfies and enables the bullet point list above.

### Aspects of Apache Kudu that are not ideal for a TSDB

Although Apache Kudu provides a lot of the features and building blocks necessary for a 
good TSDB, it's not perfect. Aspects that are not perfect as it stands today:

* The case of updating many time series with new data as time moves on has compactions as part of of the write path
  (as it's impossible to strictly write data in primary key order)
* Encoding of data is ok, but could be improved upon further as evidenced by [work done by Akumuli](https://docs.google.com/document/d/1yLsN1j8xxnm_b0oN6rFSgWOnCHP-OlJC5pBKZQwTAPc/pub)
* Stronger transactional consistency guarantees for multi-row operations would be useful for meta data operations

## Data model

### Series identity and meta data

In Serio meta data consists of the following concepts:

* **entity** - a name identifying a concept capable of having one or more time series (string).
* **metric** - a name identifying what aspect of an entity is being measured (keyword). 
  An entity has 1 time series per metric.
* **tags** - an entity can have any number of key-value pair of tags ({:key "value"}).
  Tags are used to find relevant entities, e.g. find all options that has a given underlying future.

### Series

At the moment Serio supports the following types of series:

* ***bars*** - these are bars spanning a given time period, for which *open*, *high*, *low*, *close* and
  optionally *volume* and *oi* are available.
* ***ticks*** - each row represents either a bid, offer or trade. Represented as a *price*, *volume* and *tick-type*.

## Example usage

TODO: write
