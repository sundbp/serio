(def project 'serio)
(def version "0.1.5-SNAPSHOT")

(set-env! :resource-paths #{"src"}
          :source-paths   #{"test"}
          :dependencies   '[[org.clojure/clojure         "1.9.0-alpha16"]
                            [org.apache.kudu/kudu-client "1.3.1"]
                            [manifold                    "0.1.7-alpha1"]
                            [clojure.java-time           "0.2.2"]
                            [org.clojure/tools.logging   "0.3.1"]

                            [adzerk/boot-test            "RELEASE" :scope "test"]
                            [adzerk/boot-logservice      "1.2.0" :scope "test"]
                            [sundbp/booting              "0.1.8" :scope "test"]
                            [org.apache.commons/commons-math3 "3.6.1" :scope "test"]])

(require '[sundbp.booting :as b])

(task-options!
 pom {:project     project
      :version     version
      :description "Serio is a time series database solution built on top of Apache Kudu."
      :url         "https://gitlab.com/sundbp/serio"
      :scm         {:url "https://gitlab.com/sundbp/serio"}
      :license     {"Eclipse Public License"
                    "http://www.eclipse.org/legal/epl-v10.html"}}
 repl {:init-ns 'serio.repl}
 b/deploy-release {:repo "clojars"})

;;;;;;; tasks

;; make repl task include the dev/ dir like lein does
(replace-task!
 [r repl] (fn [& xs]
            (merge-env! :resource-paths #{"dev"})
            (apply r xs)))

(deftask build []
  (comp (pom) (jar) (install)))

(require '[adzerk.boot-test :refer [test]])

(b/defreleasetask release
  (comp (pom) (jar) (b/deploy-release)))
