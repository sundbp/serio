(ns serio.repl
  (:require [adzerk.boot-logservice :as log-service]
            [clojure.tools.logging  :as log]
            [java-time :as j]
            [serio :as s]))

(alter-var-root #'log/*logger-factory* (constantly (log-service/make-factory)))

(set! *warn-on-reflection* true)

