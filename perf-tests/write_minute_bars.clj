(ns write-minute-bars
  (:require [serio :as s]
            [serio.kudu :as s.k]
            [java-time :as j]
            [manifold.deferred :as d]))

;;; random distribution fns borrowed from Anglican

(defn ^:private qualify
  "accepts a symbol, returns the qualified symbol;
  intended to be called from a macro"
  [s]
  (symbol (format "%s/%s" *ns* s)))

(def RNG
  "random number generator;
  used by Apache Commons Math distribution objects"
  (org.apache.commons.math3.random.SynchronizedRandomGenerator.
   (org.apache.commons.math3.random.Well19937c.)))

(defprotocol distribution
  "random distribution"
  (sample* [this]
    "draws a sample from the distribution")
  (observe* [this value]
    "return the probability [density] of the value"))

(defmacro defdist
  "defines distribution"
  [name & args]
  (let [[docstring parameters & args]
        (if (string? (first args))
          args
          `(~(format "%s distribution" name) ~@args))
        [bindings & methods]
        (if (vector? (first args))
          args
          `[[] ~@args])
        record-name (symbol (format "%s-distribution" name))
        variables (take-nth 2 bindings)]
    `(do
       (declare ~name)
       (defrecord ~record-name [~@parameters ~@variables]
         Object
         (toString [~'this]
           (str (list '~(qualify name) ~@parameters)))
         distribution
         ~@methods)
       (defn ~name ~docstring ~parameters
         (let ~bindings
           (~(symbol (format "->%s" record-name))
            ~@parameters ~@variables)))
       (defmethod print-method ~record-name
         [~'o ~'m]
         (print-simple (str ~'o) ~'m)))))

(defmacro ^:private from-apache
  "wraps Apache Commons Math distribution"
  [name args type [apache-name & apache-args]]
  (let [dist (gensym "dist")]
    `(defdist ~(symbol name)
       ~(format "%s distribution (imported from apache)" name)
       ~args
       [~dist (~(symbol (format "org.apache.commons.math3.distribution.%sDistribution." apache-name))
               RNG ~@apache-args)]
       (~'sample* [~'this] (.sample ~dist))
       (~'observe* [~'this ~'value]
        ~(case type
           :discrete `(~'.logProbability ~dist ~'value)
           :continuous `(~'.logDensity ~dist ~'value))))))

(from-apache normal [mean sd] :continuous
             (Normal (double mean) (double sd)))

;;;;;;;;; our actual test code

(defn contracts
  [num-markets num-contracts-per-market]
  (->> (range 1 (inc num-markets))
       (mapcat (fn [m]
                 (->> (range 1 (inc num-contracts-per-market))
                      (map (fn [c] (format "entity/%d/%d" m c))))))))

(defn generate-timestamps
  [start-time duration-fn]
  (let [minutes (iterate #(j/plus % (j/minutes 1)) start-time)
        end-time (duration-fn start-time)]
    (->> (take-while #(j/before? % end-time) minutes)
         (filter j/weekday?))))

(def open-distance (normal 0.0, (/ 0.0025 (Math/sqrt (* 24 60)))))
(def close-to-close-return (normal 0.0, (/ 0.2 (Math/sqrt (* 252 24 60)))))
(def high-to-close (normal (/ 0.0020 (* 24 60)), (/ 0.0010 (Math/sqrt (* 24 60)))))
(def bar-range (normal (/ 0.02 (* 24 60)), (/ 0.0033 (Math/sqrt (* 24 60)))))

(defn round2
  "Round a double to the given precision (number of significant digits)"
  [precision d]
  (let [factor (Math/pow 10 precision)]
    (/ (Math/round (* d factor)) factor)))

(defn random-ohlc-bar
  [prev-close ts]
  (let [open  (round2 2 (* prev-close (+ 1.0 (sample* open-distance))))
        close (round2 2 (* prev-close (Math/exp (sample* close-to-close-return))))
        high  (round2 2 (max open close (* close (+ 1.0 (sample* high-to-close)))))
        low   (round2 2 (min open close (/ high (min 1.0 (+ 1.0 (sample* bar-range))))))]
    [ts {:open open
         :high high
         :low low
         :close close}]))

(defn random-walk-bars
  [start-value timestamps]
  (:path (reduce (fn [{:keys [prev path]} ts]
                   (let [new-bar (random-ohlc-bar prev ts)]
                     {:path (conj path new-bar)
                      :prev (:close (second new-bar))}))
                 {:prev start-value
                  :path []}
                 timestamps)))

(defn generate-data
  [start-time duration-fn cs start-values]
  (let [timestamps (generate-timestamps start-time duration-fn)]
    (->> cs
         (map (fn [c]
                [c (random-walk-bars (get start-values c) timestamps)]))
         (into {}))))

(defn random-start-values
  [cs]
  (let [dist (normal 100 20)]
    (->> cs
         (map (fn [c] [c (max 2.0 (round2 2 (sample* dist)))]))
         (into {}))))

(defn perform-test
  [num-markets num-contracts-per-market]
  (let [end-time (j/zoned-date-time 2017 1 1)
        cs (contracts num-markets num-contracts-per-market)
        duration-fn #(j/plus % (j/minutes 10))]
    (with-open [client (s/connect ["10.10.10.5"])]
      (loop [period-start (j/minus end-time (j/days 2))
             start-values (random-start-values cs)]
        (if (not (j/before? period-start end-time))
          true
          (let [data (time (generate-data period-start duration-fn cs start-values))
                ;; data is of format {entity [[ts1 val1] .. [tsN valN]]}
                ;; convert it to {ts1 {entity val1}}
                to-write (reduce (fn [result [c path]]
                                   (reduce (fn [r [ts value]]
                                             (assoc-in r [ts c] value))
                                           result
                                           path))
                                 {}
                                 data)
                to-write (sort-by first j/before? to-write)]
            (with-open [session (s/new-write-session client {:flush-mode :manual-flush
                                                             :mutation-buffer-space 1200
                                                             :mutation-buffer-low-watermark 10})]
              (println "Writing data for period starting at" period-start)
              (time
               (do
                 (doseq [[ts c->v] to-write]
                   (when (j/before? ts end-time)
                     (doseq [part (partition 1024 1024 nil c->v)]
                       (let [results (doall (map (fn [[c v]]
                                                   (s/add-insert session c :ohlc :serio/minutely ts v))
                                                 part))
                             flush-result @(s/flush-session session)]
                         (doseq [r results]
                           (assert (not (.hasRowError @r))
                                   (format "Found operation error: %s" (-> (.getRowError @r)
                                                                           (.getErrorStatus)
                                                                           (.toString)))))))))
                 (println "done with period"))))
            (recur (duration-fn period-start)
                   (->> data
                        (map (fn [[c path]] [c (-> path last second :close)]))
                        (into {})))))))))

(defn read-series
  [e start end]
  (with-open [c (s/connect ["10.10.10.5"])]
    (let [results @(s/get-range c e :ohlc :serio/minutely start end)]
      (doseq [[ts v] (take 10 results)]
        (println ts v))
      results)))

(defn delete-series
  [e]
  (let [results (read-series e (j/zoned-date-time 2015 1 1) (j/zoned-date-time 2017 1 1))]
    (with-open [c (s/connect ["10.10.10.5"])
                session (s/new-write-session c {:flush-mode :manual-flush
                                                :mutation-buffer-space 1200
                                                :mutation-buffer-low-watermark 10})]
      (doseq [part (partition 1024 1024 nil results)]
        (let [rs (doall (map (fn [[ts _]]
                               (s/add-delete session e :ohlc :serio/minutely ts))
                             part))]
          @(s/flush-session session)
          (doseq [r rs]
            (assert (not (.hasRowError @r))
                    (format "Found operation error: %s" (-> (.getRowError @r)
                                                            (.getErrorStatus)
                                                            (.toString))))))))))

(defn delete-all-series
  [num-markets num-contracts-per-market]
  (let [cs (contracts num-markets num-contracts-per-market)]
    (doseq [e cs]
      (delete-series e))))

(defn start-from-scratch
  []
  (s.k/delete-all-tables ["10.10.10.5"])
  (s.k/create-db-schema ["10.10.10.5"]))
