(ns serio
  "Exposed API for working with time series data stored in Serio.
  All functions return deferred values unless explicitly specified otherwise"
  (:require [manifold.deferred :as d]

            [serio.protocols :as s.p]
            [serio.kudu :as s.k]
            [java-time :as j]))

(defn connect
  "Connect client to DB.

  master-addresses - either a single master address or a collection of addresses
  db-name          - the name of the DB to connect to (optional)

  Returns a connected client (non-deferred) on success, will throw exception on any failures."
  ([master-addresses] (connect master-addresses "default"))
  ([master-addresses db-name]
   (s.k/connect (if (coll? master-addresses) master-addresses [master-addresses])
                db-name)))

(defn close
  "Close down serio client connection.

  Undefined return value."
  [client]
  (s.p/close- client))

;;;;;;;;; write operations

(defn new-write-session
  "Create a new write session.

  A write session is a set of write operations sent to the DB as a batch.
  Exactly how batching, timeouts etc are handled can be found in the
  kudu documentation:
  https://kudu.apache.org/apidocs/org/apache/kudu/client/AsyncKuduSession.html

  session-options is of {:key value} format for all things that has a .setXXX
    method in AsyncKuduSession.

  Returns a new WriteSession"
  ([client] (new-write-session client {}))
  ([client session-options]
   (s.p/new-write-session- client session-options)))

(defn add-insert
  "Addd a new Insert operation to a given WriteSession.

  entity - a string name for the entity to insert an observation for
  metric - a keyword name for the metric to insert and observation for
  frequency - either a number of seconds indicating the periodicity of the series,
    or an alias keyword such as :serio/daily
  timestamp - the timestamp of the observation, given in a type able to be passed to
    clojure.java-time/to-millis-from-epoch
  value - the value of the observation in {:key value} format consistent with the `metric` of the call

  Returns a deferred OperationResponse for the operation just added."
  [session entity metric frequency timestamp value]
  (s.p/add-insert- session entity metric frequency timestamp value))

(defn add-upsert
  "Addd a new Upsert operation to a given WriteSession.

  entity - a string name for the entity to insert an observation for
  metric - a keyword name for the metric to insert and observation for
  frequency - either a number of seconds indicating the periodicity of the series,
    or an alias keyword such as :serio/daily
  timestamp - the timestamp of the observation, given in a type able to be passed to
    clojure.java-time/to-millis-from-epoch
  value - the value of the observation in {:key value} format consistent with the `metric` of the call

  Returns a deferred OperationResponse for the operation just added."
  [session entity metric frequency timestamp value]
  (s.p/add-upsert- session entity metric frequency timestamp value))

(defn add-update
  "Addd a new Update operation to a given WriteSession.

  entity - a string name for the entity to insert an observation for
  metric - a keyword name for the metric to insert and observation for
  frequency - either a number of seconds indicating the periodicity of the series,
    or an alias keyword such as :serio/daily
  timestamp - the timestamp of the observation, given in a type able to be passed to
    clojure.java-time/to-millis-from-epoch
  value - the value of the observation in {:key value} format consistent with the `metric` of the call

  Returns a deferred OperationResponse for the operation just added."
  [session entity metric frequency timestamp value]
  (s.p/add-update- session entity metric frequency timestamp value))

(defn add-delete
  "Addd a new Delete operation to a given WriteSession.

  entity - a string name for the entity to insert an observation for
  metric - a keyword name for the metric to insert and observation for
  frequency - either a number of seconds indicating the periodicity of the series,
    or an alias keyword such as :serio/daily
  timestamp - the timestamp of the observation, given in a type able to be passed to
    clojure.java-time/to-millis-from-epoch

  Returns a deferred OperationResponse for the operation just added."
  [session entity metric frequency timestamp]
  (s.p/add-delete- session entity metric frequency timestamp))

(defn flush-session
  "Flush WriteSession.

  Returns a deferred collection of OperationResponses for any buffered but pending operations.
  Once the deferreds have all been delivered no pending operations will be outstanding regardsless
  of flush mode."
  [session]
  (s.p/flush-session- session))

(defn close-session
  "Close a WriteSession.

  Any pending operations will be flushed before session is closed.

  Returns a deferred collection of OperationResponses for any buffered but pending operations.
  Once the deferreds have all been delivered no pending operations will be outstanding regardsless
  of flush mode."
  [session]
  (s.p/close-session- session))

;;;;;;;; reading data

(defn get-range
  "Read all values for a given series over a given range.

  client - a serio.protocols/Client instance
  entity - a string name for the entity to fetch
  metric - a keyword name for the metric to fetch
  frequency - either a number of seconds indicating the periodicity of the series,
    or an alias keyword such as :serio/daily
  start-timestamp - a timestamp of a type able to be passed to clojure.java-time/to-millis-from-epoch.
    The range is defined as EXCLUSIVE of the start-timestamp
  end-timestamp - a timestamp of a type able to be passed to clojure.java-time/to-millis-from-epoch.
    The range is defined as INCLUSIVE of the end-timestamp

  Returns a deferred value yielding a time series of the format [[ts1 val1] .. [ts2 val2]] on success,
  or yielding an exception on failure. tsX are timestamps of type java.time.Instant, and valX depends
  on the metric of the series."
  [client entity metric frequency start-timestamp end-timestamp]
  (s.p/get-range- client entity metric frequency start-timestamp end-timestamp))

(defn get-nearest
  "Read a single observation for a given series for a given point in time.

  client - a serio.protocols/Client instance
  entity - a string name for the entity to fetch
  metric - a keyword name for the metric to fetch
  frequency - either a number of seconds indicating the periodicity of the series,
    or an alias keyword such as :serio/daily
  timestamp - a timestamp of a type able to be passed to clojure.java-time/to-millis-from-epoch.
  direction - if no exact match found, indicates if the closest previous (:serio/prev) or closest
    next (:serio/next) observation will be returned
  max-time-diff - a java.time.Duration indicating the maximum distance from timestamp to be considered
    as valid if no exact match is found

  Returns a deferred value yielding a [ts val] on success, or yielding an exception on failure.
  ts is a timestamp of type java.time.Instant, and val depends on the metric of the series.
  If no match is found the deferred yields nil."
  [client entity metric frequency timestamp direction max-time-diff]
  (s.p/get-nearest- client entity metric frequency timestamp direction max-time-diff))

(defn get-exact
  "Read a single observation for a given point in time.

  Acts as get-nearest with a max-time-diff of 0, i.e. only returns a hit if
  an observation with the exact same timestamp as supplied is found."
  [client entity metric frequency timestamp]
  (get-nearest client entity metric frequency timestamp ::next (j/seconds 0)))

;;;;;;;; meta-data operations

(defn find-entities
  "Find all entities matching the supplied tags.

  tags is of the form [[:key1 \"value1\"] .. [:keyN \"valueN\"]], or any other form
    which has a guaranteed ordering of the key-value pairs when processed as a sequence.

  An entity is deemed to match if it has all [key value] pairs in the supplied tags
  (i.e. it's an intersection of the tags, not union).
  The order of the KV pairs is the sequential order that a sequence of queries will be made,
  allowing the caller to narrow down the search space by using the most restrictive KV pair
  as the first match, then the 2nd most restrictive and so forth.

  Returns a deferred set of entity1 .. entityN (or empty etc if no matching entities.)"
  [client tags]
  (s.p/find-entities- client tags))

(defn find-entities-in-parallel
  "Find all entities matching the supplied tags.

  tags is a map of the form {:key \"value\"}

  An entity is deemed to match if it has all [key value] pairs in the supplied tags map
  (i.e. it's an intersection of the tags, not union).
  The query will be performed in parallel, with 1 sub-query per KV-pair, followed by the
  intersection of results from the sub-queries. This does not allow the caller to narrow down
  the search space using domain knowledge, hence use with caution to avoid massive scans for
  any key-value pairs with a lot of entries.

  Returns a deferred set of entity1 .. entityN (or empty set if no matching entities.)"
  [client tags]
  (s.p/find-entities-in-parallel- client tags))

(defn get-tags
  "Find all tags for a given entity.
  Returns a deferred value of {:key \"value\"}"
  [client entity]
  (s.p/get-tags- client entity))

(defn set-tags
  "Set the tags for a given entity.
  tags has the format of {:key \"value\"}.
  This operation will act as an upsert of the tags provided. Any
  tags previously set but not included in this set operation will
  remain untouched.

  Returns a deferred collection of any RowErrors reported by operation
  (empty collection if succeeded without problems)"
  [client entity tags]
  (s.p/set-tags- client entity tags))

(defn entity-has-tags?
  "Check if an entity has any tags at all stored.

  Returns a deferred boolean."
  [client entity]
  (d/chain (s.p/get-tags- client entity)
           #(not (empty? %))))

(defn find-entities-and-tags
  "Find all entities matching the supplied tags, and include each entities tags in return value.
  tags is of the form [[:key1 \"value1\"] .. [:keyN \"valueN\"]], or any other form
    which has a guaranteed ordering of the key-value pairs when processed as a sequence.
  Matching is done by the same rules as specified for `find-entities`

  Returns a value of the shape {\"entity\" deferred-tags} (where tags has the same form as above and
  are the complete tag set for each entity)"
  [client tags]
  (d/chain (find-entities client tags)
           (fn [entities]
             (let [et-pairs (map (fn [e] [e (get-tags client e)]) entities)]
               (future
                 (->> et-pairs
                      (map (fn [[e ts]] [e @ts]))
                      (into {})))))))

(defn find-entities-and-tags-in-parallel
  "Parallell version of find-entities-and-tags.
  Relates to find-entities-and-tags in the same way that find-entities-in-parallel
  relates to find-entities.
  tags is a map of the form {:key1 \"value1\"}

  Returns a value of {\"entity\" deferred-tags} (where tags has the same form as above and
  are the complete tag set for each entity)"
  [client tags]
  (d/chain (find-entities-in-parallel client tags)
           (fn [entities]
             (let [et-pairs (map (fn [e] [e (get-tags client e)]) entities)]
               (future
                 (->> et-pairs
                      (map (fn [[e ts]] [e @ts]))
                      (into {})))))))
