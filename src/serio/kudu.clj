(ns serio.kudu
  (:require [serio.protocols :as s.p]
            [manifold.deferred :as d]
            [java-time :as j]
            [clojure.tools.logging :as log])
  (:import [org.apache.kudu.client
            AsyncKuduClient
            AsyncKuduClient$AsyncKuduClientBuilder
            AsyncKuduSession
            ListTabletServersResponse
            CreateTableOptions
            KuduTable
            Operation Insert Upsert Update Delete
            OperationResponse
            ExternalConsistencyMode
            SessionConfiguration$FlushMode
            DeleteTableResponse
            AsyncKuduScanner AsyncKuduScanner$AsyncKuduScannerBuilder
            KuduPredicate KuduPredicate$ComparisonOp
            RowResultIterator RowResult]
           [org.apache.kudu
            Type
            ColumnSchema
            ColumnSchema$ColumnSchemaBuilder
            ColumnSchema$Encoding
            ColumnSchema$CompressionAlgorithm
            Schema]
           [clojure.lang
            IDeref]
           [com.stumbleupon.async
            Deferred]))

(set! *warn-on-reflection* true)

(def column-schemas
  {:tags {:column->index {:key 0
                          :value 1
                          :entity 2}
          :columns [{:name :key
                     :type Type/STRING
                     :null? false
                     :pk? true
                     :encoding ColumnSchema$Encoding/DICT_ENCODING}
                    {:name :value
                     :type Type/STRING
                     :null? false
                     :pk? true
                     :encoding ColumnSchema$Encoding/DICT_ENCODING}
                    {:name :entity
                     :type Type/STRING
                     :null? false
                     :pk? true
                     :encoding ColumnSchema$Encoding/DICT_ENCODING}]}
   :bar-observations {:column->index {:entity 0
                                      :metric 1
                                      :frequency 2
                                      :timestamp 3
                                      :timestamp_calculated 4
                                      :timestamp_modified 5
                                      :open 6
                                      :high 7
                                      :low 8
                                      :close 9
                                      :volume 10
                                      :oi 11
                                      :return 12}
                      :columns [{:name :entity
                                 :type Type/STRING
                                 :null? false
                                 :pk? true
                                 :encoding ColumnSchema$Encoding/DICT_ENCODING
                                 :compression ColumnSchema$CompressionAlgorithm/LZ4}
                                {:name :metric
                                 :type Type/STRING
                                 :null? false
                                 :pk? true
                                 :encoding ColumnSchema$Encoding/DICT_ENCODING
                                 :compression ColumnSchema$CompressionAlgorithm/LZ4}
                                {:name :frequency
                                 :type Type/INT32
                                 :null? false
                                 :pk? true
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :timestamp
                                 :type Type/UNIXTIME_MICROS
                                 :null? false
                                 :pk? true
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :timestamp_calculated
                                 :type Type/UNIXTIME_MICROS
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :timestamp_modified
                                 :type Type/UNIXTIME_MICROS
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :open
                                 :type Type/DOUBLE
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :high
                                 :type Type/DOUBLE
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :low
                                 :type Type/DOUBLE
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :close
                                 :type Type/DOUBLE
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :volume
                                 :type Type/INT32
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :oi
                                 :type Type/INT32
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :return
                                 :type Type/DOUBLE
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}]}
      :ticks {:column->index {:entity 0
                              :timestamp 1
                              :timestamp_calculated 2
                              :timestamp_modified 3
                              :tick-type 4
                              :price 5
                              :volume 6}
                      :columns [{:name :entity
                                 :type Type/STRING
                                 :null? false
                                 :pk? true
                                 :encoding ColumnSchema$Encoding/DICT_ENCODING
                                 :compression ColumnSchema$CompressionAlgorithm/LZ4}
                                {:name :timestamp
                                 :type Type/UNIXTIME_MICROS
                                 :null? false
                                 :pk? true
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :timestamp_calculated
                                 :type Type/UNIXTIME_MICROS
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :timestamp_modified
                                 :type Type/UNIXTIME_MICROS
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :tick-type
                                 :type Type/STRING
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/DICT_ENCODING
                                 :compression ColumnSchema$CompressionAlgorithm/LZ4}
                                {:name :price
                                 :type Type/DOUBLE
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}
                                {:name :volume
                                 :type Type/INT32
                                 :null? true
                                 :pk? false
                                 :encoding ColumnSchema$Encoding/BIT_SHUFFLE}]}})

(defn- num-replicas
  [num-tservers]
  (min 3 (if (zero? (mod num-tservers 2))
           (dec num-tservers)
           num-tservers)))

(defn- table-options
  [num-tservers]
  {:tags {:num-replicas (num-replicas num-tservers)
          :by-hash {:columns ["key" "value"]
                    :num-buckets (* 2 num-tservers)}}
   ;; for bar-observations we are aiming down to a level of about 1min bars
   ;; For such bar series, no series for futures or options would end up longer
   ;; than ~100MB, which we don't need to time range split. Hence we only partition
   ;; the table by hash of (entity, metric, frequency)
   ;; Pros:
   ;;  - data for 1 series is stored together in 1 tablet and is small enough to have no gain from parallelizing
   ;; Cons:
   ;;  - As number of series grows, the PK space of each tablet will get bigger and bigger, which at
   ;;    some point will affect performance I assume? When?
   :bar-observations {:num-replicas (num-replicas num-tservers)
                      :by-hash {:columns ["entity" "metric" "frequency"]
                                :num-buckets (* 3 num-tservers)}}
   ;; TODO: add time partition info here and uncomment once implemented
   ;; :ticks {:num-replicas (num-replicas num-tservers)
   ;;         :by-hash {:columns ["entity"]
   ;;                   :num-buckets (* 3 num-tservers)}}
   })

(defn- wrap-deferred
  [^Deferred deferred]
  (let [r (d/deferred)]
    (doto deferred
      (.addCallbacks
       ;; success case
       (reify com.stumbleupon.async.Callback
         (call [_ value]
           (d/success! r value)))
       ;; error case
       (reify com.stumbleupon.async.Callback
         (call [_ ex]
           (d/error! r ex)))))
    r))

(defn- ->frequency ^long
  [freq]
  (if (number? freq)
    freq
    (if (keyword? freq)
      (case freq
        :serio/daily 86400 ;; (* 24 60 60)
        :serio/minutely 60 ;; 60s in a minute..
        (throw (ex-info "Unknown data frequency!" {:bad-frequency freq})))
      (throw (ex-info "Unknown type of frequency passed!" :bad-frequency freq)))))

;;;;; write operations

(defn- bar-row-write-op
  [^Operation op session entity metric frequency timestamp value]
  (let [{:keys [open high low close volume oi return]} value
        row (.getRow op)
        index-lookup (get-in column-schemas [:bar-observations :column->index])]
    (doto row
      (.addString (int (:entity index-lookup)) ^String entity)
      (.addString (int (:metric index-lookup)) (name metric))
      (.addInt    (int (:frequency index-lookup)) (->frequency frequency))
      (.addLong   (int (:timestamp index-lookup)) (j/to-millis-from-epoch timestamp)))
    (when-not (empty? value)
      (when open
        (.addDouble row (int (:open index-lookup)) (double open)))
      (when high
        (.addDouble row (int (:high index-lookup)) (double high)))
      (when low
        (.addDouble row (int (:low index-lookup)) (double low)))
      (when close
        (.addDouble row (int (:close index-lookup)) (double close)))
      (when volume
        (.addInt row (int (:volume index-lookup)) (int volume)))
      (when oi
        (.addInt row (int (:oi index-lookup)) (int oi)))
      (when return
        (.addDouble row (int (:return index-lookup)) (double return))))
    ;; TODO: deal with the monitoring/maintenance fields of timestamp_XXX
    (wrap-deferred
     (.apply ^AsyncKuduSession (:ksession session) op))))

(defmulti add-insert
  (fn [session entity metric frequency timestamp value]
    metric)
  :default ::default)

(defmethod add-insert :ohlcvoi
  [session entity metric frequency timestamp value]
  (let [op ^Insert (.newInsert ^KuduTable (get-in session [:client :bar-observations]))]
    (bar-row-write-op op session entity metric frequency timestamp value)))

(defmethod add-insert :ohlc
  [session entity metric frequency timestamp value]
  ;; just redirect to ohlcvoi, as the v and oi columns are optional
  (add-insert session entity :ohlcvoi frequency timestamp value))

(defmethod add-insert ::default
  [session entity metric frequency timestamp value]
  (assert false "not implemented!"))

(defmulti add-upsert
  (fn [session entity metric frequency timestamp value]
    metric)
  :default ::default)

(defmethod add-upsert :ohlcvoi
  [session entity metric frequency timestamp value]
  (let [op ^Upsert (.newUpsert ^KuduTable (get-in session [:client :bar-observations]))]
    (bar-row-write-op op session entity metric frequency timestamp value)))

(defmethod add-upsert :ohlc
  [session entity metric frequency timestamp value]
  ;; just redirect to ohlcvoi, as the v and oi columns are optional
  (add-upsert session entity :ohlcvoi frequency timestamp value))

(defmethod add-upsert ::default
  [session entity metric frequency timestamp value]
  (assert false "not implemented!"))

(defmulti add-update
  (fn [session entity metric frequency timestamp value]
    metric)
  :default ::default)

(defmethod add-update :ohlcvoi
  [session entity metric frequency timestamp value]
  (let [op ^Update (.newUpdate ^KuduTable (get-in session [:client :bar-observations]))]
    (bar-row-write-op op session entity metric frequency timestamp value)))

(defmethod add-update :ohlc
  [session entity metric frequency timestamp value]
  ;; just redirect to ohlcvoi, as the v and oi columns are optional
  (add-update session entity :ohlcvoi frequency value))

(defmethod add-update ::default
  [session entity metric frequency timestamp value]
  (assert false "not implemented!"))

(defmulti add-delete
  (fn [session entity metric frequency timestamp]
    metric)
  :default ::default)

(defmethod add-delete :ohlcvoi
  [session entity metric frequency timestamp]
  (let [op ^Delete (.newDelete ^KuduTable (get-in session [:client :bar-observations]))]
    (bar-row-write-op op session entity metric frequency timestamp {})))

(defmethod add-delete :ohlc
  [session entity metric frequency timestamp]
  ;; just redirect to ohlcvoi, as the v and oi columns are optional
  (add-delete session entity :ohlcvoi frequency timestamp))

(defmethod add-delete ::default
  [session entity metric frequency timestamp]
  (assert false "not implemented!"))

(defrecord KuduWriteSession [client ksession]
  s.p/WriteSession
  (add-insert- [session entity metric frequency timestamp value]
    (add-insert session entity metric frequency timestamp value))
  (add-upsert- [session entity metric frequency timestamp value]
    (add-upsert session entity metric frequency timestamp value))
  (add-update- [session entity metric frequency timestamp value]
    (add-update session entity metric frequency timestamp value))
  (add-delete- [session entity metric frequency timestamp]
    (add-delete session entity metric frequency timestamp))
  (flush-session- [session]
    (wrap-deferred
     (.flush ^AsyncKuduSession (:ksession session))))
  (close-session- [session]
    (wrap-deferred
     (.close ^AsyncKuduSession (:ksession session))))
  java.io.Closeable
  (close [session]
    (s.p/close-session- session)))

(defn- ->ExternalConsistencyMode ^ExternalConsistencyMode
  [ecm]
  (case ecm
    :client-propagated ExternalConsistencyMode/CLIENT_PROPAGATED
    :commit-wait       ExternalConsistencyMode/COMMIT_WAIT
    (throw (ex-info "Unknown external consistency mode!" {:unknown-mode ecm}))))

(defn- ->FlushMode ^SessionConfiguration$FlushMode
  [fm]
  (case fm
    :auto-flush-background SessionConfiguration$FlushMode/AUTO_FLUSH_BACKGROUND
    :auto-flush-sync       SessionConfiguration$FlushMode/AUTO_FLUSH_SYNC
    :manual-flush          SessionConfiguration$FlushMode/MANUAL_FLUSH
    (throw (ex-info "Unknown flush mode!" {:unknown-mode fm}))))

(defn- apply-session-options
  [^AsyncKuduSession ks
   {:keys [external-consistency-mode
           flush-interval
           flush-mode
           ignore-all-duplicates
           mutation-buffer-low-watermark
           mutation-buffer-space
           timeout-millis]
    :or {flush-mode :auto-flush-background}}]
  (when external-consistency-mode
    (.setExternalConsistencyMode ks (->ExternalConsistencyMode external-consistency-mode)))
  (when flush-interval
    (.setFlushInterval ks (int flush-interval)))
  (when flush-mode
    (.setFlushMode ks (->FlushMode flush-mode)))
  (when ignore-all-duplicates
    (.setIgnoreAllDuplicateRows ks (boolean ignore-all-duplicates)))
  (when mutation-buffer-low-watermark
    (.setMutationBufferLowWatermark ks (int mutation-buffer-low-watermark)))
  (when mutation-buffer-space
    (.setMutationBufferSpace ks (int mutation-buffer-space)))
  (when timeout-millis
    (.setTimeoutMillis ks (long timeout-millis)))
  ks)

;;;; read operations
(defmulti get-range
  (fn [client entity metric frequency start-timestamp end-timestamp]
    metric)
  :default ::default)

(defn- get-column-schema ^ColumnSchema
  [^KuduTable table column->index column]
  (-> (.getSchema table)
      (.getColumnByIndex (get column->index column))))

(defn- col->val
  [^RowResult row column->index column col-type]
  (let [col-index ^int (get column->index column)]
    (if (.isNull row col-index)
      nil
      (case col-type
        :double
        (.getDouble row col-index)

        :int
        (.getInt row col-index)

        :string
        (.getString row col-index)

        :timestamp
        (j/instant (.getLong row col-index))

        :long
        (.getLong row col-index)

        (throw (ex-info "Unknown column type!" {:bad-col-type col-type}))))))

(defn- scanner-query
  [client table-name add-predicates-fn to-val-fn produce-result-fn]
  (let [table (get client table-name)
        column->index (get-in column-schemas [table-name :column->index])
        scanner (-> (.newScannerBuilder ^AsyncKuduClient (:kclient client) table)
                    ^AsyncKuduScanner$AsyncKuduScannerBuilder (add-predicates-fn table column->index)
                    (.build))
        ret-val (d/deferred)
        result (atom [])
        error-cb (reify com.stumbleupon.async.Callback
                   (call [_ ex]
                     (d/error! ret-val ex)))
        add-from-row-iterator #(into % (map (partial to-val-fn column->index)) %2)]
    (doto (.nextRows ^AsyncKuduScanner scanner)
      (.addCallbacks
       ;; success case
       (reify com.stumbleupon.async.Callback
         (call [this row-iterator]
           (try
             (swap! result add-from-row-iterator row-iterator)
             (if (.hasMoreRows ^AsyncKuduScanner scanner)
               (doto (.nextRows ^AsyncKuduScanner scanner)
                 (.addCallbacks this error-cb))
               (d/success! ret-val (produce-result-fn @result)))
             (catch Throwable t
               (d/error! ret-val t)))))
       ;; error case
       error-cb))
    ret-val))

(defn- assoc-non-nil
  [m k v]
  (if (or (nil? v)
          (nil? k))
    m
    (assoc m k v)))

(defn- bar-val
  [column->index ^RowResult row]
  [(col->val row column->index :timestamp :timestamp)
   (-> {}
       (assoc-non-nil :open (col->val row column->index :open :double))
       (assoc-non-nil :high (col->val row column->index :high :double))
       (assoc-non-nil :low (col->val row column->index :low :double))
       (assoc-non-nil :close (col->val row column->index :close :double))
       (assoc-non-nil :volume (col->val row column->index :volume :int))
       (assoc-non-nil :oi (col->val row column->index :oi :int))
       (assoc-non-nil :return (col->val row column->index :return :double)))])

(defn- bar-scanner-query
  [client ^String entity metric frequency add-predicates-fn produce-result-fn]
  (scanner-query client
                 :bar-observations
                 (fn  [^AsyncKuduScanner$AsyncKuduScannerBuilder scanner-builder table column->index]
                   (-> scanner-builder
                       (.addPredicate (KuduPredicate/newComparisonPredicate
                                       (get-column-schema table column->index :entity)
                                       KuduPredicate$ComparisonOp/EQUAL
                                       entity))
                       (.addPredicate (KuduPredicate/newComparisonPredicate
                                       (get-column-schema table column->index :metric)
                                       KuduPredicate$ComparisonOp/EQUAL
                                       (name metric)))
                       (.addPredicate (KuduPredicate/newComparisonPredicate
                                       (get-column-schema table column->index :frequency)
                                       KuduPredicate$ComparisonOp/EQUAL
                                       (->frequency frequency)))
                       ^AsyncKuduScanner$AsyncKuduScannerBuilder (add-predicates-fn table column->index)))
                 bar-val
                 produce-result-fn))

(defmethod get-range :ohlcvoi
  [client ^String entity metric frequency start-timestamp end-timestamp]
  (let [pred-fn (fn [^AsyncKuduScanner$AsyncKuduScannerBuilder scanner-builder table column->index]
                  (-> scanner-builder
                      (.addPredicate (KuduPredicate/newComparisonPredicate
                                      (get-column-schema table column->index :timestamp)
                                      KuduPredicate$ComparisonOp/GREATER
                                      (j/to-millis-from-epoch start-timestamp )))
                      (.addPredicate (KuduPredicate/newComparisonPredicate
                                      (get-column-schema table column->index :timestamp)
                                      KuduPredicate$ComparisonOp/LESS_EQUAL
                                      (j/to-millis-from-epoch end-timestamp)))))
        result-fn (fn [rows] (sort-by first j/before? rows))]
    (bar-scanner-query client entity metric frequency pred-fn result-fn)))

(defmethod get-range :ohlc
  [client entity metric frequency start-timestamp end-timestamp]
  (get-range client entity :ohlcvoi frequency start-timestamp end-timestamp))

(defmethod get-range ::default
  [client entity metric frequency start-timestamp end-timestamp]
  (assert false "not implemented!"))

(defmulti get-nearest
  (fn [client entity metric frequency timestamp direction max-time-diff]
    metric)
  :default ::default)

(defmethod get-nearest :ohlcvoi
  [client entity metric frequency timestamp direction max-time-diff]
  (let [[start-ts end-ts] (if (= :serio/prev direction)
                            [(j/minus timestamp max-time-diff) timestamp]
                            [timestamp (j/plus timestamp max-time-diff)])
        pred-fn (if (= :serio/exact direction)
                  (fn [^AsyncKuduScanner$AsyncKuduScannerBuilder scanner-builder table column->index]
                    (-> scanner-builder
                        (.addPredicate (KuduPredicate/newComparisonPredicate
                                        (get-column-schema table column->index :timestamp)
                                        KuduPredicate$ComparisonOp/EQUAL
                                        (j/to-millis-from-epoch timestamp)))))
                  (fn [^AsyncKuduScanner$AsyncKuduScannerBuilder scanner-builder table column->index]
                    (-> scanner-builder
                        (.addPredicate (KuduPredicate/newComparisonPredicate
                                        (get-column-schema table column->index :timestamp)
                                        KuduPredicate$ComparisonOp/GREATER_EQUAL
                                        (j/to-millis-from-epoch start-ts)))
                        (.addPredicate (KuduPredicate/newComparisonPredicate
                                        (get-column-schema table column->index :timestamp)
                                        KuduPredicate$ComparisonOp/LESS_EQUAL
                                        (j/to-millis-from-epoch end-ts))))))
        result-fn (fn [rows]
                    (if (>= 1 (count rows))
                      (first rows)
                      (let [sorted (sort-by first j/before? rows)]
                        (if (= :serio/prev direction)
                          (last sorted)
                          (first sorted)))))]
    (bar-scanner-query client entity metric frequency pred-fn result-fn)))

(defmethod get-nearest :ohlc
  [client entity metric frequency timestamp direction max-time-diff]
  (get-nearest client entity :ohlcvoi frequency timestamp direction max-time-diff))

(defmethod get-nearest ::default
  [client entity metric frequency timestamp direction]
  (assert false "not implemented!"))

;;;; meta-data operations

(defn- entity-val
  [column->index ^RowResult row]
  (col->val row column->index :entity :string))

(defn- find-entities-for-kv-pair
  [client [k v]]
  (scanner-query client
                 :tags
                 (fn [^AsyncKuduScanner$AsyncKuduScannerBuilder scanner-builder table column->index]
                   (-> scanner-builder
                       (.addPredicate (KuduPredicate/newComparisonPredicate
                                       (get-column-schema table column->index :key)
                                       KuduPredicate$ComparisonOp/EQUAL
                                       (name k)))
                       (.addPredicate (KuduPredicate/newComparisonPredicate
                                       (get-column-schema table column->index :value)
                                       KuduPredicate$ComparisonOp/EQUAL
                                       ^String v))))
                 entity-val
                 #(into #{} %)))

(defn- find-entities-for-kv-pair-and-entities
  [client [k v] entities]
  (scanner-query client
                 :tags
                 (fn [^AsyncKuduScanner$AsyncKuduScannerBuilder scanner-builder table column->index]
                   (-> scanner-builder
                       (.addPredicate (KuduPredicate/newInListPredicate
                                       (get-column-schema table column->index :entity)
                                       (into [] entities)))
                       (.addPredicate (KuduPredicate/newComparisonPredicate
                                       (get-column-schema table column->index :key)
                                       KuduPredicate$ComparisonOp/EQUAL
                                       (name k)))
                       (.addPredicate (KuduPredicate/newComparisonPredicate
                                       (get-column-schema table column->index :value)
                                       KuduPredicate$ComparisonOp/EQUAL
                                       ^String v))))
                 entity-val
                 #(into #{} %)))

;;;; client and schema fns

(defn- tag-val
  [column->index ^RowResult row]
  [(keyword (col->val row column->index :key :string))
   (col->val row column->index :value :string)])

(defrecord SerioKuduClient [kclient tags bar-observations]
  s.p/Client
  ;; connection related
  (close- [client]
    (.close ^AsyncKuduClient (:kclient client)))

  (shutdown- [client]
    (wrap-deferred
     (.shutdown ^AsyncKuduClient (:kclient client))))

  ;; writing data
  (new-write-session- [client session-options]
    (let [ksession (.newSession ^AsyncKuduClient (:kclient client))]
      (map->KuduWriteSession {:client client
                              :ksession (apply-session-options ksession session-options)})))

  ;; reading data
  (get-range- [client entity metric frequency start-timestamp end-timestamp]
    (get-range client entity metric frequency start-timestamp end-timestamp))

  (get-nearest- [client entity metric frequency timestamp direction max-time-diff]
    (get-nearest client entity metric frequency timestamp direction max-time-diff))

  ;; meta-data related
  (find-entities- [client tags]
    (loop [result @(find-entities-for-kv-pair client (first tags))
           pairs-to-go (rest tags)]
      (if (empty? pairs-to-go)
        (d/success-deferred result)
        (recur (clojure.set/intersection result @(find-entities-for-kv-pair-and-entities
                                                  client
                                                  (first pairs-to-go)
                                                  result))
               (rest pairs-to-go)))))

  (find-entities-in-parallel- [client tags]
    (d/chain (apply d/zip (map (partial find-entities-for-kv-pair client) tags))
             (fn [entity-groups]
               (apply clojure.set/intersection entity-groups))))

  (get-tags- [client entity]
    (scanner-query client
                   :tags
                   (fn [^AsyncKuduScanner$AsyncKuduScannerBuilder scanner-builder table column->index]
                     (-> scanner-builder
                         (.addPredicate (KuduPredicate/newComparisonPredicate
                                         (get-column-schema table column->index :entity)
                                         KuduPredicate$ComparisonOp/EQUAL
                                         ^String entity))))
                   tag-val
                   #(into {} %)))
  (set-tags- [client entity tags]
    (let [ksession (-> (.newSession ^AsyncKuduClient (:kclient client))
                       (apply-session-options {:flush-mode :manual-flush}))
          column->index (get-in column-schemas [:tags :column->index])]
      (doseq [[k v] tags]
        (let [op ^Upsert (.newUpsert ^KuduTable (:tags client))
              row (.getRow op)]
          (doto row
            (.addString (int (:key column->index)) (name k))
            (.addString (int (:value column->index)) ^String v)
            (.addString (int (:entity column->index)) ^String entity))
          (.apply ^AsyncKuduSession ksession op)))
      (d/chain (wrap-deferred (.flush ^AsyncKuduSession ksession))
               (fn [op-responses]
                 (into [] (OperationResponse/collectErrors op-responses))))))

  java.io.Closeable
  (close [client]
    (s.p/close- client)))

(defn- list-tablet-servers ^ListTabletServersResponse
  [^AsyncKuduClient kc]
  (-> (.listTabletServers kc)
      (.joinUninterruptibly (.getDefaultAdminOperationTimeoutMs kc))))

(defn- num-tablet-servers
  [^AsyncKuduClient kc]
  (.getTabletServersCount (list-tablet-servers kc)))

(defn- column-schema ^ColumnSchema
  [col-spec]
  (-> (ColumnSchema$ColumnSchemaBuilder. (name (:name col-spec)) (:type col-spec))
      (.key (get col-spec :pk? false))
      (.nullable (get col-spec :null? true))
      (.encoding (get col-spec :encoding ColumnSchema$Encoding/PLAIN_ENCODING))
      (.compressionAlgorithm (get col-spec :compression ColumnSchema$CompressionAlgorithm/NO_COMPRESSION))
      (.build)))

(defn- table-schema ^Schema
  [columns]
  (Schema. (map column-schema columns)))

(defn- create-table-options ^CreateTableOptions
  [partition-info]
  (let [opts (->(CreateTableOptions.)
                (.setNumReplicas (:num-replicas partition-info)))]
    (when (contains? partition-info :by-hash)
      (.addHashPartitions opts
                          (get-in partition-info [:by-hash :columns])
                          (get-in partition-info [:by-hash :num-buckets])))
    (when (contains? partition-info :by-range)
      (assert false "not implemented yet!")
      ;; .setRangePartitionColumns
      ;; loop over ranges and call .addRangePartition for each
      )
    opts))

(defn create-db-schema
  "Create tables in kudu.
  Only expected to be run once when serio is first comissioned for a given kudu cluster.
  master-address - the address of any of the masters in the cluster.
  db-name - optional name for the serio db, to allow multiple db's to coexist in same kudu cluster.
  Returns a deferred value of true on success, will throw on failure."
  ([^java.util.List master-addresses] (create-db-schema master-addresses "default"))
  ([^java.util.List master-addresses db-name]
   (let [kc (-> (AsyncKuduClient$AsyncKuduClientBuilder. master-addresses)
                .build)]
     (d/chain (apply d/zip
                     (->> (table-options (num-tablet-servers kc))
                          (map (fn [[table-name options]]
                                 (log/info "Creating table" table-name)
                                 (wrap-deferred
                                  (.createTable kc
                                                (format "serio.%s.%s" db-name (name table-name))
                                                (table-schema (get-in column-schemas [table-name :columns]))
                                                (create-table-options options)))))))
              (fn [result]
                (doseq [t result]
                  (assert (instance? KuduTable t) "Table creation didn't yield a KuduTable result!"))
                (.close kc)
                ;; no use returning tables as we are closing the client
                true)))))

(defn delete-table
  ([^java.util.List master-addresses table-name] (delete-table master-addresses "default" table-name))
  ([^java.util.List master-addresses db-name table-name]
   (let [kc (-> (AsyncKuduClient$AsyncKuduClientBuilder. master-addresses)
                .build)]
     (d/chain (wrap-deferred
               (do
                 (log/info "Deleting table" table-name)
                 (.deleteTable kc (format "serio.%s.%s" db-name (name table-name)))))
              (fn [result]
                (.close kc)
                result)))))

(defn delete-all-tables
  "Delete all serio tables.
  Throws exception if not successful."
  ([^java.util.List master-addresses] (delete-all-tables master-addresses "default"))
  ([^java.util.List master-addresses db-name]
   (let [result (->> (keys (table-options 1))
                     (map #(delete-table master-addresses db-name %)))]
     (doseq [r result]
       (assert (instance? DeleteTableResponse @r))))))

(defn- open-table
  [^AsyncKuduClient kc db-name table-name]
  (wrap-deferred
   (.openTable kc (format "serio.%s.%s" db-name table-name))))

(defn connect
  "Connect serio client to kudu cluster.
  Returns a deferred yielding a connected client, or on any error or exception XXX" ;; TODO: docs!
  ([^java.util.List master-addresses] (connect master-addresses "default"))
  ([^java.util.List master-addresses db-name]
   (let [kc (-> (AsyncKuduClient$AsyncKuduClientBuilder. master-addresses) .build)]
     (map->SerioKuduClient {:kclient kc
                            :tags @(open-table kc db-name "tags")
                            :bar-observations @(open-table kc db-name "bar-observations")
                            ;;:ticks @(open-table kc db-name "ticks")
                            }))))
