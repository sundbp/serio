(ns serio.protocols)

(defprotocol Client
  ;; connection related
  (close- [client])
  (shutdown- [client])

  ;; writing data
  (new-write-session- [client session-options])

  ;; reading data
  (get-range- [client entity metric frequency start-timestamp end-timestamp])
  (get-nearest- [client entity metric frequency timestamp direction max-time-diff])

  ;; meta-data operations
  (find-entities- [client tags])
  (find-entities-in-parallel- [client tags])
  (get-tags- [client entity])
  (set-tags- [client entity tags]))

(defprotocol WriteSession
  (add-insert- [session entity metric frequency timestamp value])
  (add-upsert- [session entity metric frequency timestamp value])
  (add-update- [session entity metric frequency timestamp value])
  (add-delete- [session entity metric frequency timestamp])
  (flush-session- [session])
  (close-session- [session]))
